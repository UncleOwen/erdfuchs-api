# These are tests. Do we really need docstrings?
# pylint: disable=missing-docstring

import os

from dotenv import load_dotenv
from erdfuchs.generated.models.InitRequest import InitRequest

from erdfuchs.api import Api, ApiCore


def test_init() -> None:
    load_dotenv('secrets.env')

    core = ApiCore(
        os.environ['USERNAME'],
        os.environ['PASSWORD'],
    )
    api = Api(core)

    api.init(InitRequest())
