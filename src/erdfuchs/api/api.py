"""
This file contains the Api class, which has one member function for each
GTI Api endpoint.
"""
from typing import Dict

from erdfuchs.generated.api import Api as GeneratedApi
from .apicore import ApiCore


class Api(GeneratedApi):
    """
    Call the members of this class to do API calls
    """
    def __init__(self, core: ApiCore):
        super().__init__()
        self._core = core

    def _request_func(self, endpoint: str, args: Dict) -> Dict:
        """
        This is what the endpoint functions call to fire off an HTTP request.
        """
        args["version"] = 49
        return self._core.call(endpoint, args)
