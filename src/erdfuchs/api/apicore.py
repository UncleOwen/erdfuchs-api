"""
This file contains common functionality for all API calls.
"""


import base64
import hmac
from typing import Dict, cast, Optional
import uuid

import requests


def _calculate_hmac(password: str, data: bytes) -> str:
    key = bytes(password, "ascii")
    hashed = hmac.new(key, data, "sha1")
    signature = base64.b64encode(hashed.digest())
    return str(signature, "ascii")


class ApiCore:  # pylint: disable=too-few-public-methods
    """
    This class handles sending properly authenticated and properly formatted
    API calls.
    """
    def __init__(self, user: str, password: str) -> None:
        self.user = user
        self.password = password

        self.session = requests.Session()
        self.session.hooks = {
            "response": lambda r, *args, **kwargs: r.raise_for_status()
        }

    def call(self, method: str, params: Optional[Dict] = None) -> Dict:
        """
        Call an API endpoint
        :param method: which endpoint to call (the last part of the path
        component of the URL
        :param params: the parameters of the request as a python dict
        :return: the result of the request as a python dict
        """
        if not params:
            params = {}
        req = requests.Request(
            method="POST",
            url=f"https://gti.geofox.de{method}",
            json=params,
            headers={
                "Accept": "application/json",
                "geofox-auth-user": self.user,
                "geofox-auth-type": "HmacSHA1",
                "X-TraceId": str(uuid.uuid4()),
            }
        )

        prepared_req = self.session.prepare_request(req)
        prepared_req.headers["geofox-auth-signature"] = _calculate_hmac(
            self.password,
            cast(bytes, prepared_req.body),
            # This cast is safe. requests.models.PreparedRequest.prepare_body()
            # does a lot of stuff, but we are using the json code path, with
            # does make sure that the body ends up as a bytes object.
        )

        resp = self.session.send(prepared_req)

        return resp.json()
